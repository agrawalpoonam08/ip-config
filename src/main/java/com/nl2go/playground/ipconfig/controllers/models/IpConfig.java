package com.nl2go.playground.ipconfig.controllers.models;

import java.util.Objects;

public class IpConfig {

    private String ip;

    private String hostname;

    private boolean active;

    public IpConfig(String ip, String hostname, boolean active) {
        this.ip = ip;
        this.hostname = hostname;
        this.active = active;
    }

    public String getIp() {
        return ip;
    }

    public String getHostname() {
        return hostname;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IpConfig ipConfig = (IpConfig) o;
        return active == ipConfig.active &&
                Objects.equals(ip, ipConfig.ip) &&
                Objects.equals(hostname, ipConfig.hostname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, hostname, active);
    }
}
